package main

import (
	"fmt"
	"math"
)

func som(x, y float64) float64 {
	return x + y
}

func sub(x, y float64) float64 {
	return x - y
}

func mul(x, y float64) float64 {
	return x * y
}

func div(x, y float64) float64 {
	if y != 0 {
		return x / y
	}
	return math.Inf(1)
}

func pow(x, y float64) float64 {
	return math.Pow(x, y)
}

func main() {
	resultadoSoma := som(5, 3)
	fmt.Println("Soma:", resultadoSoma)

	resultadoSubtracao := sub(5, 3)
	fmt.Println("Subtração:", resultadoSubtracao)

	resultadoMultiplicacao := mul(5, 3)
	fmt.Println("Multiplicação:", resultadoMultiplicacao)

	resultadoDivisao := div(5, 3)
	fmt.Println("Divisão:", resultadoDivisao)

	resultadoPotencia := pow(5, 3)
	fmt.Println("Potência:", resultadoPotencia)
}
