package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
)

type Router struct{}

func (Router) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	url := req.URL
	path := url.Path

	if path != "/result" {
		resp := messages[404]
		respJSON, err := json.Marshal(resp)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}
		res.WriteHeader(http.StatusNotFound)
		res.Write(respJSON)
		return
	}

	if req.Method == "GET" {
		query := req.URL.RawQuery
		if len(query) < 4 {
			http.Error(res, "Query too short", http.StatusBadRequest)
			return
		}
		op := query[3:]
		res.Write([]byte("Sherek verde"))
	}
}

func runService() {
	s := &http.Server{
		Addr:         "192.168.1.10",
		Handler:      Router{},
		ReadTimeout:  4 * time.Second,
		WriteTimeout: 4 * time.Second,
	}
	log.Fatal(s.ListenAndServe())
}

var messages = map[int]string{
	404: "Not Found",
}

func main() {
	runService()
}
